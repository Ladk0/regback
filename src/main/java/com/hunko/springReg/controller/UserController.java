package com.hunko.springReg.controller;

import com.hunko.springReg.model.User;
import com.hunko.springReg.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import java.util.List;


/**
 * Class to maintain url mapping and database redaction
 */

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

    /**
     * logs into database
     */
    private static Logger logger = Logger.getLogger(UserController.class.getName());

    /**
     * provides CRUD functions
     */
    private UserService userService;

    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping
    public User create(@RequestBody User user) {
        User newUser = userService.create(user);
        logger.info("user created");
        return newUser;
    }

    @GetMapping(path = {"/{id}"})
    public User findOne(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        logger.info("User with id " + id + " retrieved");
        return user;
    }

    @PutMapping(path = {"/{id}"})
    public User update(@PathVariable("id") Long id, @RequestBody User updatedUser) {
        updatedUser.setId(id);
        userService.update(updatedUser);
        logger.info("User with id " + id + " updated");
        return updatedUser;
    }

    @DeleteMapping(path = {"/{id}"})
    public User delete(@PathVariable("id") Long id) {
        User deletedUser = userService.delete(id);
        logger.info("User with id " + id + " deleted");
        return deletedUser;
    }

    @GetMapping
    public List findAll() {
        return userService.findAll();
    }

}
