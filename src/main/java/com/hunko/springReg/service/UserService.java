package com.hunko.springReg.service;

import com.hunko.springReg.model.User;
import com.hunko.springReg.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User create(User user) {
        return userRepository.save(user);
    }

    public User delete(Long id) {
        User user = findById(id);
        if(user != null){
            userRepository.delete(user);
        }
        return user;
    }

    public List findAll() {
        return userRepository.findAll();
    }

    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    public User update(User user) {
        return userRepository.save(user);
    }
}
