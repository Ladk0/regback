package com.hunko.springReg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRegApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRegApplication.class, args);
	}

}

