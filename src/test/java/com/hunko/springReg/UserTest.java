package com.hunko.springReg;

import com.hunko.springReg.model.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private User user;

    @Before
    public void before() {
        user = new User();
    }

    @Test
    public void getId() {
        user.setId(7L);
        assertEquals(7L, (long) user.getId());
    }

    @Test
    public void setId() {
        user.setId(7L);
        assertEquals(7L, (long) user.getId());
    }

    @Test
    public void getFirstName() {
        user.setFirstName("Vladyslav");
        assertEquals("Vladyslav", user.getFirstName());
    }

    @Test
    public void setFirstName() {
        user.setFirstName("Vladyslav");
        assertEquals("Vladyslav", user.getFirstName());
    }

    @Test
    public void getLastName() {
        user.setFirstName("Hunko");
        assertEquals("Hunko", user.getFirstName());
    }

    @Test
    public void setLastName() {
        user.setFirstName("Hunko");
        assertEquals("Hunko", user.getFirstName());
    }

    @Test
    public void getEmail() {
        user.setEmail("vladyslav.o.hunko@gmail.com");
        assertEquals("vladyslav.o.hunko@gmail.com", user.getEmail());
    }

    @Test
    public void setEmail() {
        user.setEmail("vladyslav.o.hunko@gmail.com");
        assertEquals("vladyslav.o.hunko@gmail.com", user.getEmail());
    }

    @Test
    public void getPassword() {
        user.setEmail("qwerty123");
        assertEquals("qwerty123", user.getEmail());
    }

    @Test
    public void setPassword() {
        user.setEmail("qwerty123");
        assertEquals("qwerty123", user.getEmail());
    }
}
